﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacterAPI.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Discription = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    Year = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    CharactersId = table.Column<int>(type: "int", nullable: false),
                    MoviesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.CharactersId, x.MoviesId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Characters_CharactersId",
                        column: x => x.CharactersId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movies_MoviesId",
                        column: x => x.MoviesId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FirstName", "Gender", "LastName", "Picture" },
                values: new object[,]
                {
                    { 1, null, "Elijah", "Male", "Wood", "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Elijah_Wood_%2847955399861%29_%28cropped%29.jpg/800px-Elijah_Wood_%2847955399861%29_%28cropped%29.jpg" },
                    { 12, null, "Zendaya", "Female", "Coleman", "https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/Zendaya_-_2019_by_Glenn_Francis.jpg/800px-Zendaya_-_2019_by_Glenn_Francis.jpg" },
                    { 11, null, "Viggo", "Male", "Mortensen", "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Viggo_Mortensen_A_%282020%29.jpg/800px-Viggo_Mortensen_A_%282020%29.jpg" },
                    { 10, null, "Liv", "Female", "Tyler", "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Liv_Tyler_2008.jpg/800px-Liv_Tyler_2008.jpg" },
                    { 8, "Jakie Chan", "Kong-sang", "Male", "Chan", "https://upload.wikimedia.org/wikipedia/commons/8/8b/Jackie_Chan_July_2016.jpg" },
                    { 7, null, "Michael", "Male", "Caine", "https://upload.wikimedia.org/wikipedia/commons/f/f4/Sir_Michael_Caine%2C_28th_EFA_Awards_2015%2C_Berlin_%28cropped%29.jpg" },
                    { 9, null, "Ian", "Male", "McKellen", "https://upload.wikimedia.org/wikipedia/commons/d/dd/Ian_McKellen.jpg" },
                    { 5, null, "Christian", "Male", "Bale", "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Christian_Bale-7837.jpg/800px-Christian_Bale-7837.jpg" },
                    { 4, "Ice Cube", "O'Shea", "Male", "Jackson", "https://upload.wikimedia.org/wikipedia/commons/2/2c/Ice-Cube_2014-01-09-Chicago-photoby-Adam-Bielawski.jpg" },
                    { 3, null, "Chris", "Male", "Tucker", "https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Chris_Tucker_by_Gage_Skidmore.jpg/800px-Chris_Tucker_by_Gage_Skidmore.jpg" },
                    { 2, null, "Tom", "Male", "Holland", "https://upload.wikimedia.org/wikipedia/commons/f/fd/Tom_Holland_MTV_2018_%2802%29.jpg" },
                    { 6, null, "Gwyneth", "Female", "Paltrow", "https://upload.wikimedia.org/wikipedia/commons/4/42/GwynethPaltrowByAndreaRaffin2011.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Discription", "Name" },
                values: new object[,]
                {
                    { 4, "Friday is a stoner comedy film franchise created by Ice Cube and DJ Pooh.[1] The series takes place in South Central Los Angeles and follows the exploits of perpetually unemployed Craig Jones, who along with his friends and relatives, are thrust into various issues that happen to occur on a Friday.", "Friday" },
                    { 1, "The fictional character Spider-Man, a comic book superhero created by Stan Lee and Steve Ditko and featured in Marvel Comics publications, has appeared as a main character in multiple theatrical and made-for-television films.", "Spider-Man" },
                    { 2, "The Lord of the Rings is a film series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien. The films are subtitled The Fellowship of the Ring (2001), The Two Towers (2002), and The Return of the King (2003).", "The Lord of the Rings" },
                    { 3, "The Rush Hour franchise is a series of American action comedy films created by Ross LaManna and directed by Brett Ratner. All three films center around a pair of police detectives, Chief Inspector Lee and Detective James Carter, who go on their series of misadventures involving corrupt crime figures in Hong Kong and Los Angeles. ", "Rush Hour" },
                    { 5, "The Dark Knight Series is a set of three Christopher Nolan Batman movies. It includes Batman Begins (2005),The Dark Knight (2008), and The Dark Knight Rises (2012).Christian Bale, Michael Caine, Gary Oldman, Morgan Freeman, and Cillian Murphy appeared in all three movies. The Trilogy is considered by many to be one of the best of all time. Batman's grounded portrayal was universally acclaimed.", "Dark Knight Series" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "Title", "Trailer", "Year" },
                values: new object[,]
                {
                    { 1, "Jon Watts", 1, "Action", "https://upload.wikimedia.org/wikipedia/en/f/f9/Spider-Man_Homecoming_poster.jpg", "Spider-Man: Homecoming", "https://www.youtube.com/watch?v=DiTECkLZ8HM", 2017 },
                    { 2, "Jon Watts", 1, "Action", "https://upload.wikimedia.org/wikipedia/en/b/bd/Spider-Man_Far_From_Home_poster.jpg", "Spider-Man: Far From Home", "https://www.youtube.com/watch?v=Nt9L1jCKGnE", 2019 },
                    { 3, "Peter Jackson", 2, "Adventure", "https://upload.wikimedia.org/wikipedia/en/8/8a/The_Lord_of_the_Rings_The_Fellowship_of_the_Ring_%282001%29.jpg", "The Lord of the Rings: The Fellowship of the Ring", "https://www.youtube.com/watch?v=V75dMMIW2B4", 2001 },
                    { 4, "	Peter Jackson", 2, "Adventure", "https://upload.wikimedia.org/wikipedia/en/d/d0/Lord_of_the_Rings_-_The_Two_Towers_%282002%29.jpg", "The Lord of the Rings: The Two Towers", "https://www.youtube.com/watch?v=LbfMDwc4azU", 2002 },
                    { 5, "Brett Ratner", 3, "Action Comedy", "https://upload.wikimedia.org/wikipedia/en/4/49/Rush_Hour_poster.png", "Rush Hour", "https://www.youtube.com/watch?v=JMiFsFQcFLE", 1998 },
                    { 6, "Brett Ratner", 3, "Action Comedy", "https://upload.wikimedia.org/wikipedia/en/d/dc/Rush_Hour_2_poster.png", "Rush Hour 2", "https://www.youtube.com/watch?v=SCTzYY95Aw4", 2001 },
                    { 7, "F. Gary Gray", 4, "Comedy", "https://upload.wikimedia.org/wikipedia/en/2/27/Fridayposter1995.jpg", "Friday", "https://www.youtube.com/watch?v=umvFBoLOOgo", 1995 },
                    { 8, "Steve Carr", 4, "Comedy", "https://upload.wikimedia.org/wikipedia/en/e/e2/Next_Friday_Poster.jpg", "Next Friday", "https://www.youtube.com/watch?v=Uu7qV57UKSo", 2000 },
                    { 9, "Christopher Nolan", 5, "Action Adventure", "https://upload.wikimedia.org/wikipedia/en/a/af/Batman_Begins_Poster.jpg", "Batman Begins", "https://www.youtube.com/watch?v=neY2xVmOfUM", 2005 },
                    { 10, "Christopher Nolan", 5, "Action Adventure", "https://upload.wikimedia.org/wikipedia/en/8/8a/Dark_Knight.jpg", "The Dark Knight", "https://www.youtube.com/watch?v=EXeTwQWrcwY", 2008 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_MoviesId",
                table: "CharacterMovie",
                column: "MoviesId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
