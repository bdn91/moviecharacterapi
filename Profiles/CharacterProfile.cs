﻿using AutoMapper;
using System.Linq;
using MovieCharacterAPI.Models.DTOs;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Profiles
{
    public class CharacterProfile : Profile
    {

        public CharacterProfile()
        {   
            // From Character -> CharacterWithMoviesDTO
            CreateMap<Character, CharacterWithMoviesDTO>().ForMember(cdto => cdto.Movies,
                c => c.MapFrom(cm => 
                    cm.Movies.Select(v => new CharacterMovieDTO { Id = v.Id, Title = v.Title, URL = "" }).ToList()));

            // From CharacterWithMoviesDTO -> Character. Must populate with full movie afterward.
            CreateMap<CharacterWithMoviesDTO, Character>().ForMember(c => c.Movies,
                cdto => cdto.MapFrom(cm =>
                    cm.Movies.Select(v => new Movie { Id = v.Id })));

            // From Movie -> CharacterMovieDTO
            CreateMap<Movie, CharacterMovieDTO>();
        }
    }
}
