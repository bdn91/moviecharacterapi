﻿using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models.Domain;
using System.Linq;
using AutoMapper;
using MovieCharacterAPI.Models.DTOs;
using MovieCharacterAPI.Models.Dto;

namespace MovieCharacterAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            //Projection Mapping between Franchise and FranchisesDTO
            CreateMap<Franchise, FranchiseMoviesDTO>()
                .ForMember(dest => dest.MovieTitles, 
                opt => opt.MapFrom(src => 
                src.Movies.Select(m => m.Id).ToArray()))
                .ReverseMap(); //ReverseMap for bi-directional mapping      

            CreateMap<Franchise, FranchisesDTO>();
            CreateMap<FranchisesDTO, Franchise>();

        }
    }
}








/*
            CreateMap<Character, FranchisesDTO>() //map from Character to FranchiseDTO  
          .ForMember(dest => dest.Id, source => source.MapFrom(s => s.Id))
          .ForMember(dest => dest.Name, source => source.MapFrom(s => s.FirstName + ", " + s.LastName + ", " + s.Alias))
          .ForMember(dest => dest.Discription, source => source.MapFrom(s => s.Gender + ", " + s.Picture));
 */
