# Movie Charachter API
A Restful API constructed in ASP.NET Core with CRUD operations on the Movie database. <br/>
The Movie database is made in SQL and contains information about characters, movies they appear in and the franchises these movies belong to. 
Swagger Open API is used for documentation and easy interaction with the API directtly in the browser. 

## Movie Database Relationsihp Diagram 
<div align="left">
<img src="/uploads/9a335acb124740abe0a0ae1e7d8bf5b2/Database-Relationship.PNG"  width="498" height="423" alt"database relationship">
</div>
<br/>

## Getting Started
* Download or Clone to a local directory
* Copy the Server name "[Device Name]/SQLEXPRESS" from MSSMS
* Open solution in Visual Studio
* Change DefaultConnection value in moviecharacterapi\appsettings.json to the copied Server name
* Build solution then run IIS Express to start API
    * Browser will open and you can use the Swagger UI to test the endpoints 


### Contributors
Bao Nguyen <br/>
Zak Laberg <br />
Sofie Bergqvist
