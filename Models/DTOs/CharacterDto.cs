﻿namespace MovieCharacterAPI.Models.Dto
{
    public class CharacterDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
