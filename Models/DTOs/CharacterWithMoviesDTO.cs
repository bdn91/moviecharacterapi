﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.DTOs
{
    public class CharacterWithMoviesDTO
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(20)]
        public string Alias { get; set; }
        [Required]
        [MaxLength(15)]
        public string Gender { get; set; }
        [MaxLength(100)]
        public string Picture { get; set; }
        public List<CharacterMovieDTO> Movies { get; set; }
    }
}
