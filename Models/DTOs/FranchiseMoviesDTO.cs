﻿using MovieCharacterAPI.Models.Domain;
using MovieCharacterAPI.Models.DTOs;
using System.Collections.Generic;

namespace MovieCharacterAPI.DTOs
{
    public class FranchiseMoviesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public ICollection<string> MovieTitles { get; set; }

 
    }
}
