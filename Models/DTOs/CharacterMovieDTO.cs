﻿namespace MovieCharacterAPI.Models.DTOs
{
    public class CharacterMovieDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
    }
}
