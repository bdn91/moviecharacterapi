﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Models
{
    /// <summary>
    ///  Extension method on the ModelBuilder for seeding data
    /// </summary>
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            // Movie table
            modelBuilder.Entity<Movie>()
                .HasData(
                    new Movie
                    {
                        Id = 1, Title = "Spider-Man: Homecoming",
                        Genre = "Action",
                        Year = 2017,
                        Director = "Jon Watts",
                        Picture = "https://upload.wikimedia.org/wikipedia/en/f/f9/Spider-Man_Homecoming_poster.jpg",
                        Trailer = "https://www.youtube.com/watch?v=DiTECkLZ8HM",
                        FranchiseId = 1
                    },
                    new Movie
                    {
                        Id = 2,
                        Title = "Spider-Man: Far From Home",
                        Genre = "Action",
                        Year = 2019,
                        Director = "Jon Watts",
                        Picture = "https://upload.wikimedia.org/wikipedia/en/b/bd/Spider-Man_Far_From_Home_poster.jpg",
                        Trailer = "https://www.youtube.com/watch?v=Nt9L1jCKGnE",
                        FranchiseId = 1
                    },
                    new Movie
                    {
                        Id = 3,
                        Title = "The Lord of the Rings: The Fellowship of the Ring",
                        Genre = "Adventure",
                        Year = 2001,
                        Director = "Peter Jackson",
                        Picture = "https://upload.wikimedia.org/wikipedia/en/8/8a/The_Lord_of_the_Rings_The_Fellowship_of_the_Ring_%282001%29.jpg",
                        Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4",
                        FranchiseId = 2
                    },
                    new Movie
                    {
                        Id = 4,
                        Title = "The Lord of the Rings: The Two Towers",
                        Genre = "Adventure",
                        Year = 2002,
                        Director = "	Peter Jackson",
                        Picture = "https://upload.wikimedia.org/wikipedia/en/d/d0/Lord_of_the_Rings_-_The_Two_Towers_%282002%29.jpg",
                        Trailer = "https://www.youtube.com/watch?v=LbfMDwc4azU",
                        FranchiseId = 2
                    }, new Movie
                    {
                        Id = 5,
                        Title = "Rush Hour",
                        Genre = "Action Comedy",
                        Year = 1998,
                        Director = "Brett Ratner",
                        Picture = "https://upload.wikimedia.org/wikipedia/en/4/49/Rush_Hour_poster.png",
                        Trailer = "https://www.youtube.com/watch?v=JMiFsFQcFLE",
                        FranchiseId = 3
                    }, new Movie
                    {
                        Id = 6,
                        Title = "Rush Hour 2",
                        Genre = "Action Comedy",
                        Year = 2001,
                        Director = "Brett Ratner",
                        Picture = "https://upload.wikimedia.org/wikipedia/en/d/dc/Rush_Hour_2_poster.png",
                        Trailer = "https://www.youtube.com/watch?v=SCTzYY95Aw4",
                        FranchiseId = 3
                    }, new Movie
                    {
                        Id = 7,
                        Title = "Friday",
                        Genre = "Comedy",
                        Year = 1995,
                        Director = "F. Gary Gray",
                        Picture = "https://upload.wikimedia.org/wikipedia/en/2/27/Fridayposter1995.jpg",
                        Trailer = "https://www.youtube.com/watch?v=umvFBoLOOgo",
                        FranchiseId = 4
                    }, new Movie
                    {
                        Id = 8,
                        Title = "Next Friday",
                        Genre = "Comedy",
                        Year = 2000,
                        Director = "Steve Carr",
                        Picture = "https://upload.wikimedia.org/wikipedia/en/e/e2/Next_Friday_Poster.jpg",
                        Trailer = "https://www.youtube.com/watch?v=Uu7qV57UKSo",
                        FranchiseId = 4
                    }, new Movie
                    {
                        Id = 9,
                        Title = "Batman Begins",
                        Genre = "Action Adventure",
                        Year = 2005,
                        Director = "Christopher Nolan",
                        Picture = "https://upload.wikimedia.org/wikipedia/en/a/af/Batman_Begins_Poster.jpg",
                        Trailer = "https://www.youtube.com/watch?v=neY2xVmOfUM",
                        FranchiseId = 5
                    }, new Movie
                    {
                        Id = 10,
                        Title = "The Dark Knight",
                        Genre = "Action Adventure",
                        Year = 2008,
                        Director = "Christopher Nolan",
                        Picture = "https://upload.wikimedia.org/wikipedia/en/8/8a/Dark_Knight.jpg",
                        Trailer = "https://www.youtube.com/watch?v=EXeTwQWrcwY",
                        FranchiseId = 5
                    });

            // Franchise table
            modelBuilder.Entity<Franchise>()
                .HasData(
                    new Franchise
                    {
                        Id = 1,
                        Name = "Spider-Man",
                        Discription = "The fictional character Spider-Man, a comic book superhero created by Stan Lee and " +
                                      "Steve Ditko and featured in Marvel Comics publications, has appeared as a main character " +
                                      "in multiple theatrical and made-for-television films."
                    },
                    new Franchise
                    {
                        Id = 2,
                        Name = "The Lord of the Rings",
                        Discription = "The Lord of the Rings is a film series of three epic fantasy adventure films directed by Peter Jackson, " +
                                      "based on the novel written by J. R. R. Tolkien. The films are subtitled The Fellowship of the Ring (2001)," +
                                      " The Two Towers (2002), and The Return of the King (2003)."
                    },
                    new Franchise
                    {
                        Id = 3,
                        Name = "Rush Hour",
                        Discription = "The Rush Hour franchise is a series of American action comedy films created by Ross LaManna and directed by Brett Ratner. " +
                                      "All three films center around a pair of police detectives, Chief Inspector Lee and Detective James Carter, who go on their " +
                                      "series of misadventures involving corrupt crime figures in Hong Kong and Los Angeles. "
                    },
                    new Franchise
                    {
                        Id = 4,
                        Name = "Friday",
                        Discription = "Friday is a stoner comedy film franchise created by Ice Cube and DJ Pooh.[1] The series takes place in " +
                                      "South Central Los Angeles and follows the exploits of perpetually unemployed Craig Jones, who along with his " +
                                      "friends and relatives, are thrust into various issues that happen to occur on a Friday."
                    },
                    new Franchise
                    {
                        Id = 5,
                        Name = "Dark Knight Series",
                        Discription = "The Dark Knight Series is a set of three Christopher Nolan Batman movies. It includes Batman Begins (2005)," +
                                      "The Dark Knight (2008), and The Dark Knight Rises (2012).Christian Bale, Michael Caine, Gary Oldman, Morgan Freeman, " +
                                      "and Cillian Murphy appeared in all three movies. The Trilogy is considered by many to be one of the best of all time. " +
                                      "Batman's grounded portrayal was universally acclaimed."
                    });

            // Character table
            modelBuilder.Entity<Character>()
                .HasData(
                    new Character
                    {
                        Id = 1,
                        FirstName = "Elijah",
                        LastName = "Wood",
                        Gender = "Male",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Elijah_Wood_%2847955399861%29_%28cropped%29.jpg/800px-Elijah_Wood_%2847955399861%29_%28cropped%29.jpg"
                    },
                    new Character
                    {
                        Id = 2,
                        FirstName = "Tom",
                        LastName = "Holland",
                        Gender = "Male",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/f/fd/Tom_Holland_MTV_2018_%2802%29.jpg"
                    },
                    new Character
                    {
                        Id = 3,
                        FirstName = "Chris",
                        LastName = "Tucker",
                        Gender = "Male",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Chris_Tucker_by_Gage_Skidmore.jpg/800px-Chris_Tucker_by_Gage_Skidmore.jpg"
                    },
                    new Character
                    {
                        Id = 4,
                        FirstName = "O'Shea",
                        LastName = "Jackson",
                        Gender = "Male",
                        Alias = "Ice Cube",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/2/2c/Ice-Cube_2014-01-09-Chicago-photoby-Adam-Bielawski.jpg"
                    },
                    new Character
                    {
                        Id = 5,
                        FirstName = "Christian",
                        LastName = "Bale",
                        Gender = "Male",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Christian_Bale-7837.jpg/800px-Christian_Bale-7837.jpg"
                    },
                    new Character
                    {
                        Id = 6,
                        FirstName = "Gwyneth",
                        LastName = "Paltrow",
                        Gender = "Female",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/4/42/GwynethPaltrowByAndreaRaffin2011.jpg"
                    },
                    new Character
                    {
                        Id = 7,
                        FirstName = "Michael",
                        LastName = "Caine",
                        Gender = "Male",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/f/f4/Sir_Michael_Caine%2C_28th_EFA_Awards_2015%2C_Berlin_%28cropped%29.jpg"
                    },
                    new Character
                    {
                        Id = 8,
                        FirstName = "Kong-sang",
                        LastName = "Chan",
                        Gender = "Male",
                        Alias = "Jakie Chan",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/8/8b/Jackie_Chan_July_2016.jpg"
                    },
                    new Character
                    {
                        Id = 9,
                        FirstName = "Ian",
                        LastName = "McKellen",
                        Gender = "Male",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/d/dd/Ian_McKellen.jpg"
                    },
                    new Character
                    {
                        Id = 10,
                        FirstName = "Liv",
                        LastName = "Tyler",
                        Gender = "Female",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Liv_Tyler_2008.jpg/800px-Liv_Tyler_2008.jpg"
                    },
                    new Character
                    {
                        Id = 11,
                        FirstName = "Viggo",
                        LastName = "Mortensen",
                        Gender = "Male",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Viggo_Mortensen_A_%282020%29.jpg/800px-Viggo_Mortensen_A_%282020%29.jpg"
                    },
                    new Character
                    {
                        Id = 12,
                        FirstName = "Zendaya",
                        LastName = "Coleman",
                        Gender = "Female",
                        Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/Zendaya_-_2019_by_Glenn_Francis.jpg/800px-Zendaya_-_2019_by_Glenn_Francis.jpg"
                    });

            // Join table: CharacterMovie
            modelBuilder.Entity("CharacterMovie").HasData(
                new { MoviesId = 1, CharactersId = 2 },
                new { MoviesId = 1, CharactersId = 6 },
                new { MoviesId = 1, CharactersId = 12 },
                new { MoviesId = 2, CharactersId = 2 },
                new { MoviesId = 2, CharactersId = 12 },
                new { MoviesId = 3, CharactersId = 1 },
                new { MoviesId = 3, CharactersId = 10 },
                new { MoviesId = 3, CharactersId = 11 },
                new { MoviesId = 3, CharactersId = 9 },
                new { MoviesId = 4, CharactersId = 1 },
                new { MoviesId = 4, CharactersId = 10 },
                new { MoviesId = 4, CharactersId = 11 },
                new { MoviesId = 4, CharactersId = 9 },
                new { MoviesId = 5, CharactersId = 8 },
                new { MoviesId = 5, CharactersId = 3 },
                new { MoviesId = 6, CharactersId = 8 },
                new { MoviesId = 6, CharactersId = 3 },
                new { MoviesId = 7, CharactersId = 3 },
                new { MoviesId = 7, CharactersId = 4 },
                new { MoviesId = 8, CharactersId = 3 },
                new { MoviesId = 8, CharactersId = 4 },
                new { MoviesId = 9, CharactersId = 5 },
                new { MoviesId = 9, CharactersId = 7 },
                new { MoviesId = 10, CharactersId = 5 },
                new { MoviesId = 10, CharactersId = 7 });
        }
    }
}
