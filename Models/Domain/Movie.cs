﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.Domain
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        [Required]
        [MaxLength(30)]
        public string Genre { get; set; }
        [Required]
        [Range(1900, 3000)]
        public int Year { get; set; }
        [Required]
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(200)]
        public string Picture { get; set; }
        [MaxLength(200)]
        public string Trailer { get; set; }
        public int? FranchiseId { get; set; }
        public virtual Franchise Franchise { get; set; }
        public virtual ICollection<Character> Characters { get; set; }
    }
}
