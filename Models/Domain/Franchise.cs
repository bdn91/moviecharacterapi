﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.Domain
{
    public class Franchise
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [MaxLength(500)]
        public string Discription { get; set; }
        public virtual ICollection<Movie> Movies { get; set; }
        public virtual ICollection<Character> Characters { get; set; }

    }
}
