﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MovieCharacterAPI.DTOs;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using MovieCharacterAPI.Models.Dto;
using MovieCharacterAPI.Models.DTOs;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly ILogger<FranchisesController> _logger;
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        //Dependency injection to inject database context and IMapper for map two objects
        public FranchisesController(ILogger<FranchisesController> logger, MovieDbContext context, IMapper mapper)
        {
            _logger = logger;
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetch all franchises in the database
        /// </summary>
        /// <returns>Returns an array of franchises from our database</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchisesDTO>>> GetFranchises()
        {
            List<Franchise> franchises = await _context.Franchises.ToListAsync();
            return Ok(_mapper.Map<List<FranchisesDTO>>(franchises));
        }

        /// <summary>
        /// Fetch Franchise by id
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>Return Franchise array</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchisesDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _context.Franchises
                .Include(f => f.Movies)
                .FirstOrDefaultAsync(f => f.Id == id);

            FranchisesDTO franchisesDTO = _mapper.Map<FranchisesDTO>(franchise);

            _logger.LogInformation("Mapping successful");

            return franchisesDTO;
        }

        /// <summary>
        /// Fetch movies based on franchise id 
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns>Returns an array of Movies for specified franchise </returns>
        [HttpGet("{id}Movies")] //  
        public ActionResult<IEnumerable<MovieDTO>> GetMoviesFromFranchise(int id)
        {
            Franchise franchise = _context.Franchises.Include(m => m.Movies).FirstOrDefault(m => m.Id == id);
            var movieDTO = _mapper.Map<List<MovieDTO>>(franchise.Movies);

            if (franchise == null)
            {
                return NotFound();
            }

            _logger.LogInformation("Mapping successful");

            return Ok(movieDTO);
        }

        /// <summary>
        /// Fetch all characters in specified Franchise
        /// </summary>
        /// <param name="id">franchise id</param>
        /// <returns>Returns an array of characters</returns>
        [HttpGet("{id}/characters")]
        public async Task<IEnumerable<CharacterWithoutMovieDTO>> Characters(int id)
        {
            Franchise franchise = await _context.Franchises
                .Include(f => f.Characters).FirstOrDefaultAsync(m => m.Id == id);

            IEnumerable<CharacterWithoutMovieDTO> characterListDTO = _mapper
                .Map<IEnumerable<CharacterWithoutMovieDTO>>(franchise.Characters);

            return characterListDTO;

        }


        /// <summary>
        /// Update an existing Franchise
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <param name="franchisesDTO"></param>
        /// <returns>Returns a new Franchise object</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchisesDTO franchisesDTO)
        {
            if (id != franchisesDTO.Id)
            {
                return BadRequest();
            }


            Franchise franchise = new Franchise()
            {
                Id = id,
                Name = franchisesDTO.Name,
                Discription = franchisesDTO.Discription
            };

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        /// <summary>
        /// Add a new Franchise to the database
        /// </summary>
        /// <param name="franchisesDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchisesDTO franchisesDTO)
        {
            //New Franchise object
            Franchise franchise = new Franchise()
            {
                Name = franchisesDTO.Name,
                Discription = franchisesDTO.Discription
            };
            _context.Franchises.Add(franchise);

            await _context.SaveChangesAsync();

            franchisesDTO.Id = franchise.Id;


            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        /// <summary>
        /// Delete a specific Franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            FranchisesDTO franchiseDTO = new FranchisesDTO { Id = id };


            Franchise franchise = _mapper.Map<Franchise>(franchiseDTO);

            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
