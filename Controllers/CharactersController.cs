﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTOs;
using MovieCharacterAPI.Services;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        private readonly CharacterService _characterService;

        public CharactersController(MovieDbContext context, IMapper mapper, CharacterService characterService)
        {
            _context = context;
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Get all characters stored in the application.
        /// </summary>
        /// <returns>List of characters</returns>
        // GET: api/v1/characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterWithMoviesDTO>>> GetCharacters()
        {
            var characters = await _characterService.GetAll();
            characters = characters.Select(AddMovieUrls).ToList();
            return Ok(characters);
        }

        /// <summary>
        /// Get a single character by id
        /// </summary>
        /// <param name="id">Character id</param>
        /// <returns>Character</returns>
        // GET: api/v1/characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterWithMoviesDTO>> GetCharacter(int id)
        {
            var character = await _characterService.Get(id);
            if (character == null)
                return NotFound();

            AddMovieUrls(character);
            return Ok(character);
        }

        /// <summary>
        /// Add or update a character
        /// </summary>
        /// <param name="id">Id of character. Set to 0 or use POST if adding new.</param>
        /// <param name="characterDto"></param>
        /// <returns></returns>
        // PUT: api/v1/characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterWithMoviesDTO characterDto)
        {
            if (id != characterDto.Id)
            {
                return BadRequest();
            }

            var result = await _characterService.Add(characterDto);
            return result switch
            {
                CHARACTER_SERVICE_RESULT.OK => NoContent(),
                CHARACTER_SERVICE_RESULT.BAD_DATA => BadRequest(),
                _ => Ok()
            };
        }

        /// <summary>
        /// Add a completely new character.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        // POST: api/v1/characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CharacterWithMoviesDTO>> PostCharacter(CharacterWithMoviesDTO character)
        {
            var result = await _characterService.Add(character);
            
            return result switch
            {
                CHARACTER_SERVICE_RESULT.NOT_FOUND => NotFound(),
                CHARACTER_SERVICE_RESULT.BAD_DATA => BadRequest(),
                _ => CreatedAtAction(nameof(GetCharacter), new { id = character.Id }, character)
            };
        }

        /// <summary>
        /// Get the movies a character has starred in.
        /// </summary>
        /// <param name="id">Character id.</param>
        /// <returns>A list of movies</returns>
        // GET: api/v1/characters/{characterId}/movies
        [HttpGet("{id}/movies")]
        public async Task<ActionResult> GetCharacterMovies(int id)
        {
            var movies = await _characterService.GetMovies(id);
            if (movies == null)
            {
                return NotFound();
            }

            return Ok(movies);
        }

        /// <summary>
        /// Add a movie to a character
        /// </summary>
        /// <param name="characterId">Character id.</param>
        /// <param name="movieId">Movie id.</param>
        /// <returns></returns>
        // POST: api/v1/characters/{characterId}/movies
        [HttpPost("{characterId}/movies")]
        public async Task<ActionResult> PostMovieToCharacter(int characterId, [FromBody] int movieId)
        {
            var character = await _context.Characters.Include(c => c.Movies).Where(c => c.Id == characterId).FirstOrDefaultAsync();
            if (character == null)
            {
                return NotFound();
            }

            var movie = await _context.Movies.FindAsync(movieId);
            if (movie == null) {
                return NotFound();
            }

            var charMovieIds = character.Movies.Select(movie => movie.Id).ToArray();
            if(charMovieIds.Contains(movieId))
                return NoContent();
            
            character.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Delete a character
        /// </summary>
        /// <param name="id">Character id.</param>
        /// <returns></returns>
        // DELETE: api/v1/characters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var result = await _characterService.Delete(id);
            return result switch
            {
                CHARACTER_SERVICE_RESULT.NOT_FOUND => NotFound(),
                _ => NoContent()
            };
        }


        private CharacterWithMoviesDTO AddMovieUrls(CharacterWithMoviesDTO character)
        {
            // TODO: Debug w/ MovieController present. Presumably will construct correct URL then.
            foreach(var movie in character.Movies)
            {
                movie.URL = Url.Action($"{movie.Id}", "movies");
            }

            return character;
        }
    }
}
