﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using MovieCharacterAPI.Models.DTOs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services
{
    public enum CHARACTER_SERVICE_RESULT
    {
        BAD_DATA,
        NOT_FOUND,
        OK
    }

    public class CharacterService
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public CharacterService(MovieDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        public async Task<CharacterWithMoviesDTO> Get(int id)
        {
            var character = await _context.Characters.Include(c => c.Movies).Where(c => c.Id == id).FirstOrDefaultAsync();

            if (character == null)
            {
                return null;
            }
            return _mapper.Map<CharacterWithMoviesDTO>(character);
        }

        // TODO: use out param to return result & result code
        public async Task<List<CharacterMovieDTO>> GetMovies(int id)
        {
            // Find character
            var character = await _context.Characters.Include(c => c.Movies).Where(c => c.Id == id).FirstOrDefaultAsync();
            if (character == null)
            {
                return null;
            }

            // Move movies over to DTO to prevent overposting
            var movies = character.Movies.Select(movie => _mapper.Map<CharacterMovieDTO>(movie)).ToList();
            return movies;
        }

        public async Task<List<CharacterWithMoviesDTO>> GetAll()
        {
            var characters = await _context.Characters.Include(c => c.Movies).ToListAsync();
            var characterDtos = characters.Select(c => _mapper.Map<CharacterWithMoviesDTO>(c)).ToList();

            return characterDtos;
        }

        public async Task<CHARACTER_SERVICE_RESULT> Delete(int id)
        {
            var character = await _context.Characters.Where(c => c.Id == id).FirstOrDefaultAsync();
            if (character == null)
            {
                return CHARACTER_SERVICE_RESULT.NOT_FOUND;
            }

            // Automagically removes entries in linking table too
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
            return CHARACTER_SERVICE_RESULT.OK;
        }

        public async Task<CHARACTER_SERVICE_RESULT> Add(CharacterWithMoviesDTO characterDto)
        {
            // Tag character as not existing
            characterDto.Id = 0;
            return await AddOrUpdate(characterDto);
        }

        public async Task<CHARACTER_SERVICE_RESULT> AddOrUpdate(CharacterWithMoviesDTO characterDto)
        {
            // Map DTO to domain model
            Character newCharacter = _mapper.Map<Character>(characterDto);
            var newMovieIds = newCharacter.Movies.Select(movie => movie.Id).ToList();

            // Check if we are updating or adding
            var characterExists = CharacterExists(characterDto.Id);

            // Get movies already added; we detach them from ctx to let it know that they need not be added
            var oldMovies = new List<Movie>();
            if (characterExists)
            {
                // Get the old movies
                var oldCharacter = await _context.Characters.Include(c => c.Movies).Where(c => c.Id == characterDto.Id).FirstAsync();
                oldMovies = oldCharacter.Movies.ToList();

                // Remove movies not present in newCharacter from oldCharacter, so that they are removed from linking table.
                var moviesToRemove = oldMovies.Where(movie => !newMovieIds.Contains(movie.Id)).ToList();
                moviesToRemove.ForEach(movie => oldCharacter.Movies.Remove(movie));

                // Save removal, detach old character to prepare for updating it with new info.
                _context.SaveChanges();
                _context.Entry(oldCharacter).State = EntityState.Detached;
            }

            // Make sure the added movies actually exist, then convert Movie ids to full Movie models
            var newMovies = new List<Movie>();
            foreach (var movieId in newMovieIds)
            {
                if (!MovieExists(movieId))
                    return CHARACTER_SERVICE_RESULT.BAD_DATA;

                // Id to full movie
                var fullMovie = _context.Movies.Find(movieId);

                // Detach if exists (or Attach - both signal not to track/update the entity)
                if (oldMovies.Contains(fullMovie))
                    _context.Movies.Attach(fullMovie);
                //_context.Entry(fullMovie).State = EntityState.Detached;

                // Add always
                newMovies.Add(fullMovie);
            }

            // Movies currently contains "dummy" movies with only Id; remove them
            newCharacter.Movies.Clear();

            // Add the movies proper
            newMovies.ForEach(movie => newCharacter.Movies.Add(movie));

            // Mark for update, then save
            if (characterExists)
                _context.Entry(newCharacter).State = EntityState.Modified;
            else
                _context.Characters.Add(newCharacter);

            try
            {
                await _context.SaveChangesAsync();

                // If called from Add, we'd like the new id out to provide
                // a location with CreatedAtAction
                characterDto.Id = newCharacter.Id;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(characterDto.Id))
                {
                    return CHARACTER_SERVICE_RESULT.NOT_FOUND;
                }
                else
                {
                    throw;
                }
            }
            return CHARACTER_SERVICE_RESULT.OK;
        }
    }
}
